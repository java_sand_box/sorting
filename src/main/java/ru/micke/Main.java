package ru.micke;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.ListIterator;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println(">Введите последовательность чисел или sort для начала сортировки");
        ArrayList <Integer> list =  inputData();
        System.out.println("Входная последовательность:" + list.toString());
        sort(list, inputSortType(), inputSortOrder());
        System.out.println("Выходная последовательность:" + list.toString());
    }
    private static ArrayList <Integer> inputData() {
        ArrayList <Integer> list = new ArrayList <Integer>();
        String str = inputString();
        while (str.contains("sort") == false) {
            list.add(getInt(str));
            str = inputString();
        }
        return list;
    }

    public static String inputString(){
                return scanner.next();
    }

    public static Integer getInt(String str){
        Integer _new;
        try {
            _new = Integer.parseInt(str);
        } catch (NumberFormatException  exp){
            System.out.println("Ошибка ввода, введите число ещё раз");
            _new = getInt(inputString());
        }
        return _new;
    }
    enum SortType{
        BUBBLE
    }
    enum SortOrder{
        UPWARD,
        DOWNWARD
    }
    private static SortType inputSortType() {
        System.out.println(">Введите тип сортировки:bubble = 1");
        String input = inputString();
        if(input.equals("1")){
            return SortType.BUBBLE;
        } else {
            System.out.println("Ошибка: неверный тип сортировки, введите снова ");
            return inputSortType();
        }
    }
    private static SortOrder inputSortOrder() {
        System.out.println(">Введите порядок сортировки: upward = 1; downward = 2");
        String input = inputString();
        if(input.equals("1")){
            return SortOrder.UPWARD;
        } else if(input.equals("2")) {
            return SortOrder.DOWNWARD;
        } else {
            System.out.println("Ошибка: неверный порядок сортировки, введите снова");
            return inputSortOrder();
        }
    }

    public static void sort(ArrayList <Integer> inst, SortType type, SortOrder order){
        switch (type){
            case BUBBLE:
                sortByBubble(inst, order);
        }
    }
    protected static void sortByBubble(ArrayList <Integer> inst, SortOrder order){
        if(inst.size() == 1) {
            return;
        }
        for(int i =0; i<inst.size(); ++i) {
            ListIterator <Integer> second = inst.listIterator(1);
            ListIterator <Integer> first = inst.listIterator(0);
            for(int j = 1; j< inst.size() - i; ++j) {
                if(isNeedSwap(first.next(), second.next(), order)) {
                    Collections.swap(inst,j,j-1);
                }
            }
        }
    }

    public static boolean isNeedSwap(Integer first, Integer second, SortOrder order) {
        return (order == SortOrder.UPWARD) ? second < first
                                           : first  < second;
    }
}